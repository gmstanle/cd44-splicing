# Filter single-cell splice junction files from STAR junction mapper to a list of desired splice junctions
# Here is it applied to a list of highly-detected junctions in the Cd44 gene 
# 180919 Geoff Stanley, Quake Lab

# coding: utf-8

import pandas as pd
import numpy as np
import os.path
INPUTPATH='/storage/maca_SJ_mergedFiles/'
OUTPUTPATH='/storage/maca-splicing/cd44/SJ_files/'

# list of file names only, not including path!
FILELISTPATH='/storage/maca-splicing/cd44/filelist.txt'

flist=open(FILELISTPATH, 'r').read().splitlines()

SJs_to_get=pd.read_csv('/storage/maca-splicing/cd44/spliceSummary_CD44_goodSites.csv')
ids_to_get=set(SJs_to_get['id'])

for i, file in enumerate(flist):
    output_filename = OUTPUTPATH+"cd44_"+file
    if(not os.path.isfile(output_filename)):

        try: # some files seem to be empty or not have normal structure
            sjtab = pd.read_table(INPUTPATH+file, header=None).loc[:, (0, 1, 2, 3, 4, 5, 6)]
            sjtab.columns = ['chr','b1','b2','strand','intron_motif','annotated','read_count']
            sjtab['id']=sjtab['chr'] + "_" + sjtab['b1'].map(str) + "_" + sjtab['b2'].map(str)
            filterArr=sjtab['id'].isin(ids_to_get)
            numSites=sum(filterArr)
            if(numSites > 0):
                sjtab_good=sjtab.loc[filterArr]
                
                sjtab_good.to_csv(OUTPUTPATH+"cd44_"+file, index=False,columns= ['chr','b1','b2','strand','intron_motif','annotated','read_count'])

        except:
            continue
            
    if(i % 1000 == 0):
        print(i)
        
    

