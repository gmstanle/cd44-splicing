---
title: "R Notebook"
output:
  html_document:
    df_print: paged
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 

```{r}
require(data.table)
sj.all=fread("../sj-files/maca_sj_summary.csv")
```
```{r}
setkey(sj.all, chr, id)
```

chr2_102828611_102831329 is an exon inclusion
chr2_102828611_102853014 is a many-exon exclusion event

chr2_102831537_102832482 is exon inclusion
chr2_102831537_102853014 is a many-exon exclusion event
```{r}
sj.chr2=sj.all[chr=="chr2"]
sj.cd44=sj.chr2[(start > 102809142) & (stop < 102903665)]

sj.cd44[num_cells > 60]
sj.cd44.good <- sj.cd44[num_cells > 60]
nrow(sj.cd44.good)
fwrite(sj.cd44.good, file = "spliceSummary_CD44_goodSites.csv")
```


Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).

The preview shows you a rendered HTML copy of the contents of the editor. Consequently, unlike *Knit*, *Preview* does not run any R code chunks. Instead, the output of the chunk when it was last run in the editor is displayed.
