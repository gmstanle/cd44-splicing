import pandas as pd
import numpy as np
import os.path
INPUTPATH='/storage/maca-splicing/cd44/SJ_files/'
OUTPUTPATH='/storage/maca-splicing/cd44/data/'
OUTPUTFILE='cd44_spliceJunctions.csv'

from os import listdir
from os.path import isfile, join
cell_SJ_files = [f for f in listdir(INPUTPATH) if isfile(join(INPUTPATH, f))]

for i, f in enumerate(cell_SJ_files):
    sjtab = pd.read_table(join(INPUTPATH, f))
    if(i==0):
        combined_df = sjtab 
    else:
        combined_df = combined_df.append(sjtab, ignore_index=True)

    if(i % 1000 == 0):
        print(i)
        print(f)


combined_df.to_csv(join(OUTPUTPATH, OUTPUTFILE))
