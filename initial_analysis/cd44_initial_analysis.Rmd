---
title: "R Notebook"
output: html_notebook
---

```{r}
require(Seurat)
require(dplyr)
require(cowplot)

metadata <- read.csv("~/tabula-muris-vignettes/data/TM_facs_metadata.csv")
metadata
```

```{r}
data=read.csv("../data/cd44_spliceJunctions_v2.csv", row.names = 1)

data$cell.name <- substr(data$cell, 6, regexpr("_S[0-9]+.merged.SJ.out.tab", data$cell)-1)
data$cell.name=make.names(data$cell.name)
sum(unique(data$cell.name) %in% metadata$cell)
data$cell=NULL
data$cell <- data$cell.name
data$cell.name=NULL
head(data)
```

Add splice junctions per cell type
```{r}
rownames(metadata) <- metadata$cell
data$cell_ontology_class <- metadata[data$cell,"cell_ontology_class"]

data <- data[!is.na(data$cell_ontology_class), ]
data$tissue <-  metadata[data$cell,"tissue"]
# data$tissue <-  metadata[data$cell,"]


data$celltype_tiss <- paste0(data$tissue, "_", data$cell_ontology_class)
data$id=paste0(data$chr, "_", data$b1, "_", data$b2)

```
Get total splice site counts per tissue_celltype

```{r}
summary <- data %>% group_by(tissue, cell_ontology_class,id) %>% summarise(counts=sum(read_count), ncells=sum(read_count>0))
ncells.subtype <- metadata %>% group_by(tissue, cell_ontology_class) %>% summarise(n=n())

summary=summary %>% inner_join(ncells.subtype, by = c("tissue","cell_ontology_class"))
summary <- summary %>% filter(n > 5)
summary$frac_cells <- summary$ncells / summary$n
summary



summary.splicesites <- data %>% group_by(chr, b1, b2, id) %>% summarise(nCounts.SS=sum(read_count), nCells.SS=sum(read_count>0))
write.table(summary.splicesites, file="../data/cd44_spliceSite_summary.tsv", sep="\t", row.names = F, col.names = T)
require(reshape2)
summary$celltype_tiss <- paste0(summary$tissue, "_", summary$cell_ontology_class)

sum.counts <- dcast(summary, id ~ celltype_tiss, value.var = "counts")
sum.counts[is.na(sum.counts)] <- 0
rownames(sum.counts) <- sum.counts$id

sum.counts <- sum.counts[, 2:ncol(sum.counts)]
sum.counts <- as.matrix(sum.counts)
write.table(sum.counts, file="../data/cd44_celltype_sumCounts.tsv", sep="\t", row.names = T, col.names = T)


tiss.summary <- metadata %>% distinct(tissue, cell_ontology_class) 
tiss.summary$celltype_tiss <- paste0(tiss.summary$tissue, "_", tiss.summary$cell_ontology_class)



frac.cells <- dcast(summary, id ~ celltype_tiss, value.var = "frac_cells")
frac.cells[is.na(frac.cells)] <- 0
rownames(frac.cells) <- frac.cells$id

frac.cells <- frac.cells[, 2:ncol(frac.cells)]
frac.cells <- as.matrix(frac.cells)
write.csv(frac.cells, file="../data/cd44_celltype_fracCells.csv")

```

Plot fractions of cells expressing a site
```{r}
summary.splicesites %>% filter(b1==102828611)
frac.cells.ss <- melt(frac.cells[grepl("102828611",rownames(frac.cells)), ])
frac.cells.ss <- frac.cells.ss %>% filter(grepl("Pancreas|Liver|Muscle", Var2))
# 
# frac.cells.ss <- frac.cells.ss %>% inner_join(tiss.summary, by="celltype_tiss")
ggplot(frac.cells.ss, aes(Var1, value, fill=Var2))+
  geom_bar(stat = 'identity')+
  facet_wrap(~Var2)+
  theme(legend.position="none")
```

# Cell-type-specific junctions
```{r}
mat.counts <- dcast(data, id ~ cell, fill = 0, value.var = "read_count")
mat.counts


```





